# -*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):

    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)
        self.children.append(modules.ModelList(
            title=u'Администрирование',
            column=1,
            models=('django.contrib.auth.models.User',
                    'django.contrib.sites.models.Site',
                    'django.contrib.flatpages.models.FlatPage',
            )
        ))

        self.children.append(modules.ModelList(
            title=u'Рекламные места',
            column=1,
            models=('gorod.billboards.models.City',
                    'gorod.billboards.models.Format',
                    'gorod.billboards.models.View',
                    'gorod.billboards.models.Billboard',
            )
        ))


