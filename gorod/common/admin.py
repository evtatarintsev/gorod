__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin
from django.db import models

from ckeditor.fields import CKEditorWidget


class WSGIFlatPageAdmin(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, WSGIFlatPageAdmin)