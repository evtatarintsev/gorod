#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
import os.path
import uuid
import urllib2 as url
from django.db import models
from django.core.urlresolvers import reverse


from django.conf import settings
import json


def dir_for_billboard(instance, filename):
        ext = filename.split('.')[-1]
        filename = '%s.%s'%(uuid.uuid4(), ext)
        return os.path.join('billboards', str(instance.billboard.pk), filename)


class City(models.Model):
    name = models.CharField(u'Название', max_length=50)

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'

    def __unicode__(self):
        return self.name


class Format(models.Model):
    name = models.CharField(u'Обозначение', max_length=30)

    class Meta:
        verbose_name = u'Формат'
        verbose_name_plural = u'Форматы'

    def __unicode__(self):
        return self.name


class View(models.Model):
    name = models.CharField(u'Обозначение', max_length=30)

    class Meta:
        verbose_name = u'Вид'
        verbose_name_plural = u'Виды'

    def __unicode__(self):
        return self.name


class Billboard(models.Model):
    city = models.ForeignKey(City, verbose_name=u'Город')
    format = models.ForeignKey(Format, verbose_name=u'Формат')
    view = models.ForeignKey(View, verbose_name=u'Вид')
    address = models.CharField(u'Адрес', max_length=50)
    sides = models.IntegerField(u'Сторон', default=1, choices=((1, '1'), (2, '2')))
    light = models.BooleanField(u'Свет', default=False)
    price = models.DecimalField(u'Цена', max_digits=8, decimal_places=2)
    lat = models.FloatField(default=0, blank=True, editable=False)
    long = models.FloatField(default=0, blank=True, editable=False)

    class Meta:
        verbose_name = u'Рекламное место'
        verbose_name_plural = u'Рекламные места'

    def get_absolute_url(self):
        return reverse("billboards:detail", kwargs={'pk':self.pk})

    def save(self, **kwargs):
        self.long, self.lat = get_coordinate(self.city, self.address)
        super(Billboard, self).save( **kwargs)
class Image(models.Model):
    billboard = models.ForeignKey(Billboard, verbose_name=u'Рекламный щит', related_name='images')
    image = models.ImageField(u'Изображение', upload_to=dir_for_billboard, max_length=255)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'


def get_coordinate(city, address):
    geocode = city.name + ',+' + address.replace(' ', '+')
    request = settings.MAPS_GEOCODER_JSON_URL + u'&geocode=' + geocode
    try:
        response = url.urlopen(request.encode('utf-8')).read()
        response = json.loads(response)
        pos = response['response']\
                      ['GeoObjectCollection']\
                      ['featureMember'][0]\
                      ['GeoObject']\
                      ['Point']\
                      ['pos'].split()

        return pos
    except:
        return (None, None)