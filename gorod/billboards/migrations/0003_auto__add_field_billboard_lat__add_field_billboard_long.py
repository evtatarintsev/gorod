# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Billboard.lat'
        db.add_column(u'billboards_billboard', 'lat',
                      self.gf('django.db.models.fields.FloatField')(default=0, blank=True),
                      keep_default=False)

        # Adding field 'Billboard.long'
        db.add_column(u'billboards_billboard', 'long',
                      self.gf('django.db.models.fields.FloatField')(default=0, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Billboard.lat'
        db.delete_column(u'billboards_billboard', 'lat')

        # Deleting field 'Billboard.long'
        db.delete_column(u'billboards_billboard', 'long')


    models = {
        u'billboards.billboard': {
            'Meta': {'object_name': 'Billboard'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billboards.City']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billboards.Format']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'}),
            'light': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'long': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'sides': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'view': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billboards.View']"})
        },
        u'billboards.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'billboards.format': {
            'Meta': {'object_name': 'Format'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'billboards.image': {
            'Meta': {'object_name': 'Image'},
            'billboard': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['billboards.Billboard']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'})
        },
        u'billboards.view': {
            'Meta': {'object_name': 'View'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['billboards']