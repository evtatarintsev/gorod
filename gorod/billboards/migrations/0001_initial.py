# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'City'
        db.create_table(u'billboards_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'billboards', ['City'])

        # Adding model 'Format'
        db.create_table(u'billboards_format', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'billboards', ['Format'])

        # Adding model 'View'
        db.create_table(u'billboards_view', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'billboards', ['View'])

        # Adding model 'Billboard'
        db.create_table(u'billboards_billboard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billboards.City'])),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billboards.Format'])),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sides', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('light', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
        ))
        db.send_create_signal(u'billboards', ['Billboard'])

        # Adding model 'Image'
        db.create_table(u'billboards_image', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('billboard', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['billboards.Billboard'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'billboards', ['Image'])


    def backwards(self, orm):
        # Deleting model 'City'
        db.delete_table(u'billboards_city')

        # Deleting model 'Format'
        db.delete_table(u'billboards_format')

        # Deleting model 'View'
        db.delete_table(u'billboards_view')

        # Deleting model 'Billboard'
        db.delete_table(u'billboards_billboard')

        # Deleting model 'Image'
        db.delete_table(u'billboards_image')


    models = {
        u'billboards.billboard': {
            'Meta': {'object_name': 'Billboard'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billboards.City']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billboards.Format']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'light': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'sides': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'billboards.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'billboards.format': {
            'Meta': {'object_name': 'Format'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'billboards.image': {
            'Meta': {'object_name': 'Image'},
            'billboard': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['billboards.Billboard']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'})
        },
        u'billboards.view': {
            'Meta': {'object_name': 'View'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['billboards']