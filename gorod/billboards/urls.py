__author__ = 'Evgeniy Tatarintsev'
from django.conf.urls import patterns, include, url

from views import BillboardDetailView, BillboardListView


urlpatterns = patterns('',
    url(r'^$', BillboardListView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/$', BillboardDetailView.as_view(), name='detail'),
)
