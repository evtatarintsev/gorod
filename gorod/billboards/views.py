#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.views.generic import ListView, DetailView
from django.db.models import Q
from .models import Billboard


class BillboardListView(ListView):
    def get_queryset(self):
        boards = Billboard.objects.all()
        q = self.request.GET.get('q', None)
        if q:
            boards = boards.filter(Q(city__name__icontains=q)|
                                    Q(format__name__icontains=q)|
                                    Q(view__name__icontains=q)|
                                    Q(address__icontains=q)|
                                    Q(price__icontains=q))
        return boards


class BillboardDetailView(DetailView):
    model = Billboard