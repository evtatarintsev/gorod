__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin

from .models import City, Billboard, Image, Format, View


class CityAdmin(admin.ModelAdmin):
    pass


class FormatAdmin(admin.ModelAdmin):
    pass


class ViewAdmin(admin.ModelAdmin):
    pass


class ImageInline(admin.TabularInline):
    model = Image


class BillboardAdmin(admin.ModelAdmin):
    inlines = (ImageInline, )
    list_display = ('pk', 'city', 'format', 'view', 'light', 'address', 'price')
    list_filter = ('city', 'format', 'view', 'light', 'sides', )
    search_fields = ('address', 'city__name', 'format__name', 'view__name', 'price')

admin.site.register(City, CityAdmin)
admin.site.register(Billboard, BillboardAdmin)
admin.site.register(Format, FormatAdmin)
admin.site.register(View, ViewAdmin)